from django.shortcuts import render
import json
from django.http import Http404
from django.http import HttpResponse
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.views.decorators.csrf import csrf_exempt
from collections import OrderedDict
from django.http import JsonResponse
from django.forms.models import model_to_dict
from .models import *
from django.contrib.auth import authenticate
from django.contrib.sessions.models import Session

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

def get_request_msg(request):
    rqst_msg = request.body.decode("utf-8")
    return rqst_msg

def parse_json_data(data_str):
    try:
        json_data = json.loads(data_str, object_pairs_hook=OrderedDict)

    except ValueError as json_err_msg:
        return ("", 'Error parsing the request body')

    return (json_data, "")

@csrf_exempt
def handle_cousre_info(request):
    if request.method != "POST":
        return HttpResponse(status=404)

    request_body = get_request_msg(request)
    request_data,error_message = parse_json_data(request_body)

    if not request_data:
        return HttpResponse(status=404)

    # I didn't know if login and session API are required
    # Hence I manually create a new session for the API
    user = authenticate(username="ayush", password="Messi123")

    email = request_data["email"]
    token_request = request.META.get('HTTP_AUTHORIZATION')
    request_token = token_request.split(" ")[1]
    print(request_token)

    token = Token.objects.get_or_create(user=user)
    user_token = token[0].key
    print(user_token)

    if not token:
        HttpResponse(status=404)
    if request_token != user_token:
        return HttpResponse(status=404)

    user = User.objects.get(email=email)
    if not user:
        return HttpResponse(status=404)

    instructor = Instructor.objects.get(user=user)
    if not instructor:
        return HttpResponse(status=404)

    response_dict = {}
    ans = []
    instructorappointment = InstructorAppointment.objects.filter(instructorID=instructor)
    for ins in instructorappointment:
        if ins.active:
            courseins = CourseInstructor.objects.filter(instructorID=ins)
            print(courseins)
            for courseins_object in courseins:
                course = courseins_object.courseID
                print(course)
                try:
                    temp_dict = {}
                    temp_dict["link"] = "app.acadoinformatics.com/syllabus/portal/" + course.classCode
                    temp_dict["title"] = course.title
                    if courseins_object.coord == 1:
                        temp_dict["cooins_cord"] = "Instructor"
                    else:
                        temp_dict["cooins_cord"] = "Co-ordinator"
                    ans.append(temp_dict)
                    print(ans)
                except:
                    return HttpResponse(status=404)
    
    response_dict["Course"] = ans
    return JsonResponse(response_dict)