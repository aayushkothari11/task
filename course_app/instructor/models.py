from django.db import models
import datetime
from django.contrib.auth.models import User


class Course(models.Model):
    # id - Django stores id by default and is unique
    classCode = models.CharField(max_length=20)
    courseNumber = models.CharField(max_length=8)
    section = models.IntegerField()
    title = models.CharField(max_length=100)
    Units = models.IntegerField()
    level = models.IntegerField()
    startDate = models.DateField(default=datetime.date.today)
    endDate = models.DateField(default=datetime.date.today)
    irregular = models.SmallIntegerField()
    dow = models.CharField(max_length=7)
    ohtime = models.TextField()
    location = models.CharField(max_length=200)
    description = models.TextField()
    courseNotes = models.TextField()
    descriptionAssignment = models.TextField()
    evalInfo = models.TextField()
    instructorBio = models.TextField()
    askCopy = models.SmallIntegerField()
    priorCourseID = models.IntegerField()
    programID = models.IntegerField()
    semesterID = models.IntegerField()
    hasBeenEdited = models.SmallIntegerField()
    gradingBreakdown = models.TextField()
    useCondensedSchedule = models.SmallIntegerField()
    templateID = models.IntegerField()
    extrafields = models.TextField()
    programYear = models.CharField(max_length=250)


class Instructor(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='user')
    photo = models.CharField(max_length=100)
    office = models.CharField(max_length=100)
    usstatus = models.SmallIntegerField()
    address1 = models.CharField(max_length=200)
    address2 = models.CharField(max_length=200)
    citystatezip = models.CharField(max_length=200)
    country = models.CharField(max_length=100)
    degree = models.CharField(max_length=250)


class InstructorAppointment(models.Model):
    # id unique of django
    type_instructor = models.SmallIntegerField()
    active = models.SmallIntegerField()
    vendor_number = models.CharField(max_length=12)
    homedepartmentID = models.IntegerField()
    instructorID = models.ForeignKey(Instructor, on_delete=models.CASCADE, related_name="instructor")
    fullTime = models.SmallIntegerField()
    tenureStatus = models.SmallIntegerField()
    title = models.CharField(max_length=300)
    limitedEngagementEligible = models.SmallIntegerField()
    academicRank = models.CharField(max_length=100)


class CourseInstructor(models.Model):
    # id unique of django
    coord = models.SmallIntegerField()
    eval_field = models.SmallIntegerField()
    courseID = models.ForeignKey(Course, on_delete=models.CASCADE, related_name="course")
    instructorID = models.ForeignKey(InstructorAppointment, on_delete=models.CASCADE, related_name="instructorapp")
    officeHours = models.TextField()
    preferredContact = models.TextField()
