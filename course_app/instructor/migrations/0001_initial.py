# Generated by Django 3.2.7 on 2021-09-21 12:06

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('classCode', models.CharField(max_length=20)),
                ('courseNumber', models.CharField(max_length=8)),
                ('section', models.IntegerField()),
                ('title', models.CharField(max_length=100)),
                ('Units', models.IntegerField()),
                ('level', models.IntegerField()),
                ('startDate', models.DateField(default=datetime.date.today)),
                ('endDate', models.DateField(default=datetime.date.today)),
                ('irregular', models.SmallIntegerField()),
                ('dow', models.CharField(max_length=7)),
                ('ohtime', models.TextField()),
                ('location', models.CharField(max_length=200)),
                ('description', models.TextField()),
                ('courseNotes', models.TextField()),
                ('descriptionAssignment', models.TextField()),
                ('evalInfo', models.TextField()),
                ('instructorBio', models.TextField()),
                ('askCopy', models.SmallIntegerField()),
                ('priorCourseID', models.IntegerField()),
                ('programID', models.IntegerField()),
                ('semesterID', models.IntegerField()),
                ('hasBeenEdited', models.SmallIntegerField()),
                ('gradingBreakdown', models.TextField()),
                ('useCondensedSchedule', models.SmallIntegerField()),
                ('templateID', models.IntegerField()),
                ('extrafields', models.TextField()),
                ('programYear', models.CharField(max_length=250)),
            ],
        ),
        migrations.CreateModel(
            name='Instructor',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('photo', models.CharField(max_length=100)),
                ('office', models.CharField(max_length=100)),
                ('usstatus', models.SmallIntegerField()),
                ('address1', models.CharField(max_length=200)),
                ('address2', models.CharField(max_length=200)),
                ('citystatezip', models.CharField(max_length=200)),
                ('country', models.CharField(max_length=100)),
                ('degree', models.CharField(max_length=250)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='InstructorAppointment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type_instructor', models.SmallIntegerField()),
                ('active', models.SmallIntegerField()),
                ('vendor_number', models.CharField(max_length=12)),
                ('homedepartmentID', models.IntegerField()),
                ('fullTime', models.SmallIntegerField()),
                ('tenureStatus', models.SmallIntegerField()),
                ('title', models.CharField(max_length=300)),
                ('limitedEngagementEligible', models.SmallIntegerField()),
                ('academicRank', models.CharField(max_length=100)),
                ('instructorID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='instructor', to='instructor.instructor')),
            ],
        ),
        migrations.CreateModel(
            name='CourseInstructor',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('coord', models.SmallIntegerField()),
                ('eval_field', models.SmallIntegerField()),
                ('officeHours', models.TextField()),
                ('preferredContact', models.TextField()),
                ('courseID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='course', to='instructor.course')),
                ('instructorID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='instructorapp', to='instructor.instructorappointment')),
            ],
        ),
    ]
